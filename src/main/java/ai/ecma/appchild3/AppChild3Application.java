package ai.ecma.appchild3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppChild3Application {

    public static void main(String[] args) {
        SpringApplication.run(AppChild3Application.class, args);
    }

}
